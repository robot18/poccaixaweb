*** Settings ***
Library     SeleniumLibrary
Library     FakerLibrary


*** Variables ***
${BotaoAcessar}                                        id=AcessoAConta
${IrParaConteudo}                                      link=Ir para conteúdo
${Usuario}                                             id=nomeUsuario
${BotaoContinuarLogin}                                 id=btnLogin
${AlertaUsuarioNaoCadastrado}                          //div[@id="msgErroGenerico"]
${AlertaUsuarioNaoCadastradoValidacao}                 X5 - USUÁRIO NÃO CADASTRADO
${AlertaUsuarioNaoInformado}                           //div[@class="msg"]
${AlertaUsuarioNaoInformadoValidacao}                  Obrigatório informar o usuário.
${AcessarPJ}                                           id=tpPessoaJuridica


*** Keywords ***
Acessar Minha Conta
  Wait Until Element Is Visible     ${BotaoAcessar}
  Click Element                     ${BotaoAcessar}

Ir para Conteudo
  Wait Until Element Is Visible    ${IrParaConteudo}
  Click Link                       ${IrParaConteudo}

Usuario Não Cadastrado
    Wait Until Element Is Visible     ${Usuario}
    ${nomeUsuario}=                   FakerLibrary.Name
    Input Text                        ${Usuario}    ${nomeUsuario}
    Click Button                      ${BotaoContinuarLogin}

Confirmar Mensagem Usuário não Cadastrado
    Wait Until Element Is Visible     ${AlertaUsuarioNaoCadastrado}
    Sleep    2
    Element Text Should Be            ${AlertaUsuarioNaoCadastrado}   ${AlertaUsuarioNaoCadastradoValidacao}

Usuário Não Informado
    Wait Until Element Is Visible     ${Usuario}
    Click Button                      ${BotaoContinuarLogin}
    Wait Until Element Is Visible     ${AlertaUsuarioNaoInformado}
    Sleep    2
    Element Text Should Be            ${AlertaUsuarioNaoInformado}    ${AlertaUsuarioNaoInformadoValidacao}

Acessar Minha Conta PJ
    Wait Until Element Is Visible     ${AcessarPJ}
    Click Element                     ${AcessarPJ}
