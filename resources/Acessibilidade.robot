*** Settings ***
Library     SeleniumLibrary

Resource            AcessarMinhaConta.robot

*** Variables ***
${Acessibilidade}    id=item2
${Link Conheça}      link=Conheça
${Botao Fechar}      //article[@id='knowMoreModal1']/div/button/span

*** Keywords ***
Acessar Acessibilidade
  Wait Until Element Is Visible     ${Acessibilidade}
  Click Element                     ${Acessibilidade}
  Click Link                        ${Link Conheça}

Fechar Acessibilidade
  Click Element    ${Botao Fechar}
