*** Settings ***
Documentation       Principal

Library             SeleniumLibrary

Resource            AcessarMinhaConta.robot
Resource            Acessibilidade.robot
Resource            Loterias.robot

*** Variables ***

${URL}                          https://www.caixa.gov.br/Paginas/home-caixa.aspx
${NAVEGADOR}                    chrome


*** Keywords ***

Abrir Navegador
  Open Browser        ${URL}  ${NAVEGADOR}
  Maximize Browser Window
  Aceitar Termo de Privacidade

Aceitar Termo de Privacidade
  Wait Until Element Is Visible      //button[@class="submit-d submit-blue btn blue"]
  Click Button                      //button[@class="submit-d submit-blue btn blue"]


Capturar tela
    Capture Page Screenshot

Fechar Navegador
  Sleep               3
  Close Browser
