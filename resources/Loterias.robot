*** Settings ***
Library     SeleniumLibrary


*** Variables ***
${LinkLoterias}                    link=LOTERIAS CAIXA
${BotaoAposte}                    //section[@id='itemQuatro']/article/div/div/div/div[2]/p[4]/a
${BotaoSim}                      id=botaosim
${BotaoNao}                      id=botaonao
${BotaoMega}                    //ul[2]/li/a/figure/h3
${BotaoComplete}                //button[@id='completeojogo']
${BotaoCarinho}                //button[@id='colocarnocarrinho']
${MensagemAlerta}              id=div_alertas
${MensagemAlertaValidar}       ${SPACE}Aposta inserida no carrinho com sucesso.
#${MensagemAlertaValidar}       Aposta inserida no carrinho com sucesso.


*** Keywords ***
Acessar Loterias

    Wait Until Element Is Visible       ${LinkLoterias}
    Click Element                       ${LinkLoterias}

Apostar Loterias Maior de 18
    Wait Until Element Is Visible     ${BotaoAposte}
    Click Link                        ${BotaoAposte}
    Click Link                        ${BotaoSim}
    Wait Until Element Is Visible     ${BotaoMega}
    Click Element                     ${BotaoMega}
    Sleep    3
    Click Button                      ${BotaoComplete}
    Click Button                      ${BotaoCarinho}


Validar Mensagem de Alerta
    Wait Until Element Is Visible      ${MensagemAlerta}
    Element Text Should Be             ${MensagemAlerta}       ${MensagemAlertaValidar}


    #Adicionar ao Carrinho
    #Wait Until Element Is Visible     //button[@id='irparapagamento']/span
    #Click Element                    //button[@id='irparapagamento']/span

Apostar Loterias Menor de 18
    Wait Until Element Is Visible    ${BotaoAposte}
    Click Link                       ${BotaoAposte}
    Click Link                       ${BotaoNao}
