*** Settings ***
Library     SeleniumLibrary


*** Variables ***
${LinkTrabalhadores}          link=PARA TRABALHADORES
${LinkConhecaBeneficios}      link=Conheça seus benefícios ▶
#${LinkSabiaMais}              //a[@id="ctl00_ctl58_g_fd7b9401_d0b8_4e66_a261_250a9fc2307b_hlBotao"]
${LinkSabiaMais}              link=Saiba mais

*** Keywords ***
Acessar Para Trabalhadores
    Wait Until Element Is Visible     ${LinkTrabalhadores}
    Click Element                     ${LinkTrabalhadores}
    Wait Until Element Is Visible     ${LinkConhecaBeneficios}
    Click Link                        ${LinkConhecaBeneficios}
    Sleep    2
    Wait Until Element Is Visible     ${LinkSabiaMais}
    Click Link                        ${LinkSabiaMais}
