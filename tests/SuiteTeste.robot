*** Settings ***
Documentation     Suite de teste Acesso ao Site da CAIXA

Resource        ${EXECDIR}/resources/Base.robot
Resource        ../resources/AcessarMinhaConta.robot
Resource        ../resources/Acessibilidade.robot
Resource        ../resources/Loterias.robot
Resource        ../resources/Trabalhadores.robot

Test Setup          Abrir Navegador
Test Teardown       Fechar Navegador

*** Test Cases ***

Cenario 1: Acessar Minha Conta
    Acessar Minha Conta
    Ir para Conteudo
    Capturar tela

Cenario 2: Usuario Não Cadastrado
    Acessar Minha Conta
    Usuario Não Cadastrado
    Capturar tela
    Confirmar Mensagem Usuário não Cadastrado
    Sleep   2
    Capturar tela

Cenario 3: Usuário Não Informado
    Acessar Minha Conta
    Usuário Não Informado
    Capturar tela

Cenario 4: Acessar Minha Conta PJ
    Acessar Minha Conta
    Acessar Minha Conta PJ
    Capturar tela

Cenario 5: Acessar Acessibilidade
    Acessar Minha Conta
    Acessar Acessibilidade
    Capturar tela

Cenario 6: Acessar Para Trabalhadores
    Acessar Para Trabalhadores
    Capturar tela

Cenario 7: Loterias
    Acessar Loterias
    Capturar tela
    Apostar Loterias Maior de 18
    Capturar tela
    Validar Mensagem de Alerta
    Capturar tela

Cenario 8: Loterias Menor de 18
    Acessar Loterias
    Apostar Loterias Menor de 18
    Capturar tela
